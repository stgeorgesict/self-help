Welcome to the how to guide for St. Georges ICT coding group.
-------------------------------------------------------------

First we need to signup for [bitbucket.org](https://bitbucket.org/)



Next you will need to join the private group on bitbucket:
----------------------------------------------------------

[Bitbucket Group](https://bitbucket.org/stgeorgesict)



Familiarise yourself with the projects and readme's of those projects to understand what they are about

Once this is done, you will need to install the following programs:




//Sublime Text
==============
[Download](http://www.sublimetext.com/2)

This is the most important tool, the text editor

Make sure sublime text is the default to open any text/coding file you have or are working on




//Source Tree
=============
[Download](http://www.sourcetreeapp.com/download/)

This is required to push updates to bitbucket.org (the cloud)

Login to bitbucket with sourcetree and "clone" the existing repositories into your local workspace

When you have made changes to code, that you are completely happy with and have tried/tested, you can commit the change to the repo by selecting commit, and then the files and pushing them to Master/Origin "Origin" to be accessible elsewhere.

Use pull to download code pushed bby other users

Be careful not to make dangerous changes without testing them first




//Node.js
=========
[Download](http://nodejs.org/download/)

This is required to rune any .js files/modules

Test installed by tring "npm" || "node" which should return "Undefined" || (err)


//Useful node modules:
----------------------

//nodemon
---------
```npm install -g nodemon```

Used for development, allows easy restart for changes using "rs" and watches for changes

//Launching node with .bat
--------------------------
You can use node or nodemon (if installed) to start the app.
```node app.js```
```nodemon app.js```

//Set title of the cmd prmpt
```title Application```

//Remember to pause the application as to read error messages
```pause```


//Python
========

If Python is your language of choice, you will need to  install it to your computer by downloading the [Python Shell](http://www.python.org/download/releases/2.7.6)

Stick to release version 2.7.6 - this is because version 3 is not a production release and has some compatibility issues with some modules.

Python Shell is the default program you will use for debugging syntax errors, because it defines them for you. However, in any other case you will use Sublime Text and run the script in Command Prompt

To open a .py app in IDLE(Python Shell) instead of CMD, right click and select 'Open in IDLE', then click 'Run' and select 'Run Module'.

Some useful modules:

[setuptools](https://pypi.python.org/pypi/setuptools)
Used for installing more complicated custom modules

[SocketIO Client](https://pypi.python.org/pypi/socketIO-client)
Used for connecting to a server using the SocketIO protocol

//C++
=====
//html
======
//css
=====
//php
=====
//Readme.md
===========
Read the following guide on how to format .md files quickly and easily:
[How-to](http://daringfireball.net/projects/markdown/basics)

```var version = "0.0.0.2"```


